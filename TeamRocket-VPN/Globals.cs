﻿using DiscordRPC.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamRocket_VPN
{
    public static class Globals
    {
        public static DiscordRPC.DiscordRpcClient RPCClient = new DiscordRPC.DiscordRpcClient("987213738664943627");
        public static readonly DiscordRPC.RichPresence RichPresence = new DiscordRPC.RichPresence
        {
            State = "Not Connected",
            Assets = new DiscordRPC.Assets
            {
                LargeImageKey = "1",
                SmallImageKey = "favicon"
            },
            Buttons = new DiscordRPC.Button[]
            {
                new DiscordRPC.Button
                {
                    Label = "Website",
                    Url = "https://teamrocket.gay"
                },
                new DiscordRPC.Button
                {
                    Label = "GitLab",
                    Url = "https://gitlab.com/blastingoff"
                }
            }
        };

        public static void SetRPC()
        {
            var settings = SettingsManager.Load();
            if (settings.DiscordRPC)
            {
                RPCClient.SetPresence(RichPresence);
            }
        }
    }
}
