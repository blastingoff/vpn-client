﻿using System;
using System.IO;
using System.Windows.Forms;
using TeamRocket_VPN.Forms;
using TeamRocket_VPN.Properties;

namespace TeamRocket_VPN
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            CustomMessageBox.Show("Team Rocket VPN",
                Resources.UnhandledException);
            File.AppendAllText(Strings.ErrorLogs, (e.ExceptionObject as Exception)?.ToString());
        }
    }
}
